import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

public class TestGina_Bot extends TelegramLongPollingBot {

    private static final Logger logger = LoggerFactory.getLogger(TestGina_Bot.class);

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()){
            String message_text = update.getMessage().getText();
            long chat_id = update.getMessage().getChatId();


            SendMessage message = new SendMessage()
                    .setChatId(chat_id)
                    .setText(message_text+" too");

            try {
                execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String getBotUsername() {
        return "TestGina_Bot";
    }

    @Override
    public String getBotToken() {
        return "473815519:AAG32WyTRvJovsp1Du1wwc5ktG38Yd0O-Mw";
    }
}
